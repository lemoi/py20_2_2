# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции

import os

migrations = 'Migrations'
current_dir = os.path.dirname(os.path.abspath(__file__))


def file_contains(path, phrase):
    with open(path) as file:
        return phrase in file.read()

def files_contains(path, files, phrase):
    founds_files = []
    for file in files:
        if file_contains(os.path.join(path, file), phrase):
            founds_files.append(file)

    return founds_files


# print(__file__)
# print(current_dir)
if __name__ == '__main__':
    # ваша логика
    path = os.path.join(current_dir, migrations)
    if not os.path.exists(path):
        # папка с миграциями отсутствует предполагаем что запрос выполняется для 2-го задания (доп.), т.е. скрипт лежит в папке Migrations и непосредственно в ней и нужно делать поиск
        path = current_dir

    files = [file for file in os.listdir(os.path.join(path))]
    # print('\n'.join(files))

    files_sql = [file for file in files if file.endswith('.sql') and os.path.isfile(os.path.join(path, file))]
    # files_sql = files_sql[0: 20]
    # print('\n'.join(files_sql))

    while len(files_sql):
        phrase = input('\nВведите строку: ')

        files_sql = files_contains(path, files_sql, phrase);
        print('\n'.join([os.path.join(migrations, file) for file in files_sql]))
        print('Всего: %i' % len(files_sql))

import subprocess
import os

# current_path = os.path.dirname(os.path.abspath(__file__))
# path_to_utility = os.path.join(current_path, 'convert')
# path_to_image = os.path.join(current_path, 'convert', 'Source')
# image_name = '47c8937f8ae4966e356219a9015b1c67.jpg'

# print(os.getcwd())

print(os.getcwd())

def resize_image(image_name, size=200):
  dir_result = 'Result'
  if not os.path.exists(dir_result):
    os.makedirs(dir_result)

  path_result = os.path.join(dir_result, image_name)
  if os.path.exists(path_result):
    # os.remove(path_result)
    raise FileExistsError(f'Файла {os.path.abspath(path_result)} уже существует')

  command = [
    'convert',
    os.path.join('Source', image_name),
    '-resize',
    str(size),
    path_result
  ]
  # print(' '.join(command))

  return subprocess.call(command)

# command = 'convert face-04.jpg -resize 200 output.jpg'
# subprocess.call(command)

# os.chder('Source')
for file in os.listdir('Source'):
  if os.path.isfile(os.path.join('Source', file)):
    try:
      resize_image(file)
      print(f'Файл обработан: {file}')
    except FileExistsError as e:
      print(f'Файл пропущен, так как уже есть файл с тем же именем в папке с результатами: \n  {file}')

# print(resize_image('face-04.jpg'))


